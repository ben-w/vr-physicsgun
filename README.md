# VR Physics Gun in Unity 2020.3 using Steam VR Plugin

A recreation of the Physics Gun from Garry's Mod in VR. The physics gun can pick up and move objects in the scene which have a rigidbody.
This uses Steam VR Plugin and Unity 2020.3


# Links
- [Youtube Video](https://youtu.be/cKAwkK2ZaEI)
- [Source Code (Gitlab)](https://gitlab.com/ben-w/vr-physicsgun)
- [Portfolio Post](https://ben-w.com/posts/vr-physics-gun/)