using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace VRPhysicsGun
{
    public class ColliderListener : MonoBehaviour
    {
        public UnityAction<Collider> OnTriggerEnterAction;
        public UnityAction<Collider> OnTriggerExitAction;

        private void OnTriggerEnter(Collider other)
        {
            OnTriggerEnterAction?.Invoke(other);
        }

        private void OnTriggerExit(Collider other)
        {
            OnTriggerExitAction?.Invoke(other);
        }
    }
}