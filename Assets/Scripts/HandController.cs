using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace VRPhysicsGun
{
    public class HandController : MonoBehaviour
    {
        public Vector3 Velocity { get { return _rigidbody.velocity; } }
        public Quaternion Rotation { get { return _rigidbody.rotation; } }
        public Vector3 Position { get { return _controller.transform.position; } }
        public Rigidbody Rigidbody { get { return _rigidbody; } }

        [SerializeField] private string _intractableTag = "Intractable";
        [SerializeField] private SteamVR_Action_Boolean _pickup;
        [SerializeField] private SteamVR_Action_Boolean _trigger;
        [SerializeField] private SteamVR_Action_Vector2 _touchPad;
        [SerializeField] private SteamVR_Action_Boolean _toggleMode;

        [SerializeField] private SteamVR_Behaviour_Pose _controller;
        [SerializeField] private ColliderListener _colliderListener;
        [SerializeField] private GameObject _model;
        [SerializeField] private BoxCollider _collider;
        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private Rigidbody _jointRigidbody;

        private List<Transform> _pickableObjects = new List<Transform>();
        private IInteractable _itemHeld;

        private void Awake()
        {
            _colliderListener.OnTriggerExitAction += BoxOnTriggerExit;
            transform.position = _controller.transform.position;
            transform.rotation = _controller.transform.rotation;
            CreateJoint();
        }

        private void OnEnable()
        {
            _pickup.onChange += PickupChange;
            _trigger.onChange += TriggerChange;
            _touchPad.onChange += TouchPadChange;
            _toggleMode.onChange += ToggleModeChange;
        }

        private void OnDisable()
        {
            _pickup.onChange -= PickupChange;
            _trigger.onChange -= TriggerChange;
            _touchPad.onChange -= TouchPadChange;
            _toggleMode.onChange -= ToggleModeChange;
        }

        private void PickupChange(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource, bool newState)
        {
            if (newState)
                Pickup();
            else
                Drop();
        }

        private void TriggerChange(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource, bool newState)
        {
            if (_itemHeld != null)
                _itemHeld.TriggerChanged(newState);
        }

        private void TouchPadChange(SteamVR_Action_Vector2 fromAction, SteamVR_Input_Sources fromSource, Vector2 axis, Vector2 delta)
        {
            if (_itemHeld != null)
                _itemHeld.TouchPadChanged(axis);
        }

        private void ToggleModeChange(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource, bool newState)
        {
            if (_itemHeld != null)
                _itemHeld.MenuButtonChanged(newState);
        }

        private void Drop()
        {
            if (_itemHeld == null)
                return;

            _itemHeld.Dropped(this);
            _itemHeld = null;
            ShowController();
        }

        private void Pickup()
        {
            if (_pickableObjects.Count == 0)
                return;

            float distance = float.MaxValue;
            float lastDistance;
            Transform objectToPickup = null;
            for (int i = 0; i < _pickableObjects.Count; i++)
            {
                lastDistance = Vector3.Distance(transform.position, _pickableObjects[i].position);
                if (lastDistance < distance)
                {
                    distance = lastDistance;
                    objectToPickup = _pickableObjects[i];
                }
            }

            _itemHeld = objectToPickup.GetComponent<IInteractable>();
            if (_itemHeld == null)
            {
                Debug.LogError($"{objectToPickup.name} has intractable tag but doesn't use IInteractable");
                return;
            }

            _collider.isTrigger = true;
            HideController();
            _itemHeld.PickedUp(this);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(_intractableTag))
            {
                _pickableObjects.Add(other.transform);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (_pickableObjects.Contains(other.transform))
            {
                _pickableObjects.Remove(other.transform);
            }
        }

        private void HideController()
        {
            _model.SetActive(false);
            _collider.gameObject.SetActive(false);
        }

        private void ShowController()
        {
            _model.SetActive(true);
            _collider.gameObject.SetActive(true);
        }

        private void CreateJoint()
        {
            FixedJoint joint = gameObject.AddComponent<FixedJoint>();
            joint.connectedBody = _jointRigidbody;
        }

        private void Update()
        {
            //_rigidbody.velocity = (_controller.transform.position - _rigidbody.position) * 25f;
            //_rigidbody.angularVelocity = Vector3.zero;
            //_rigidbody.angularVelocity = (transform.rotation.eulerAngles - _rigidbody.rotation.eulerAngles);
        }

        private void BoxOnTriggerExit(Collider other)
        {
            if (!other.CompareTag(_intractableTag))
                return;
            _collider.isTrigger = false;
        }
    }
}