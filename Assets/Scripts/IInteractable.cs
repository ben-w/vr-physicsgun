using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;


namespace VRPhysicsGun
{
    public interface IInteractable
    {
        public void PickedUp(HandController controller);

        public void Dropped(HandController controller);

        public void TriggerChanged(bool state);

        public void TouchPadChanged(Vector2 position);

        public void MenuButtonChanged(bool state);
    }
}