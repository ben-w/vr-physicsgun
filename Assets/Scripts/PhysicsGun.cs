using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using DG.Tweening;
using DG.Tweening.Plugins.Options;
using DG.Tweening.Core;
using UnityEngine.Events;
using System;

namespace VRPhysicsGun
{
    public class PhysicsGun : MonoBehaviour, IInteractable
    {
        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private LineRenderer _lineRenderer;

        [SerializeField] private float _targetMaxDistance = 20;
        [SerializeField] private float _targetMinDistance = 1;
        [SerializeField] private Transform _barrelPosition;
        [SerializeField] private float[] _targetSpeeds;
        [SerializeField] private float _targetMoveSpeed = 0.5f;
        [SerializeField] private float _objectSnapSpeed = 0.5f;
        [SerializeField] private float _rotationSpeed = 2f;
        [SerializeField] private float _throwForce = 3f;

        private bool _isHeld = false;
        private bool _isHoldingObject = false;
        private FixedJoint _joint;
        private Vector3 _targetCurrentDistance = new Vector3(0, 0, 1);
        private Vector3 _targetTargetPosition;
        private bool _triggerDown = false;
        private FixedJoint _targetFixedJoint;
        private Rigidbody _heldRigidbody;
        private TweenerCore<Vector3, Vector3, VectorOptions> _heldTweener;
        private Rigidbody _lastHitRigidbody;
        private Rigidbody[] _rayTargets;
        private Transform _targetPoint;
        private Rigidbody _targetRigidbody;

        private void Start()
        {
            _rayTargets = new Rigidbody[_targetSpeeds.Length];
            for (int i = 0; i < _rayTargets.Length; i++)
            {
                _rayTargets[i] = new GameObject($"Ray Target {i}").AddComponent<Rigidbody>();
                _rayTargets[i].isKinematic = true;

                if (i + 1 == _rayTargets.Length)
                {
                    _targetPoint = _rayTargets[i].transform;
                    _targetRigidbody = _rayTargets[i];
                }
            }

            _lineRenderer.positionCount = _targetSpeeds.Length + 2;
        }

        public void Dropped(HandController controller)
        {
            _rigidbody.useGravity = true;
            _isHeld = false;
            _lineRenderer.enabled = false;
            _triggerDown = false;

            Destroy(_joint);

            if (_isHoldingObject)
                DropObject();
        }

        public void PickedUp(HandController controller)
        {
            if (_isHeld)
                return;

            _rigidbody.useGravity = false;
            _isHeld = true;
            transform.rotation = controller.Rotation;
            transform.position = controller.Position;
            CreateJoint(controller.Rigidbody);
        }

        public void TriggerChanged(bool state)
        {
            _triggerDown = state;

            if (!state && _isHoldingObject)
            {
                DropObject();
            }
        }

        public void TouchPadChanged(Vector2 position) => _targetCurrentDistance.z += position.y;

        public void MenuButtonChanged(bool state) { }

        private void CreateJoint(Rigidbody rigidbody)
        {
            _joint = gameObject.AddComponent<FixedJoint>();
            _joint.connectedBody = rigidbody;
        }

        private void Update()
        {
            CheckLength();
            if (_isHeld)
            {
                ClampTarget();
                MoveTarget();
                DrawLine();

                if (_triggerDown && !_lineRenderer.enabled)
                    _lineRenderer.enabled = true;
                else if (!_triggerDown && _lineRenderer.enabled)
                    _lineRenderer.enabled = false;


                if (!_isHoldingObject && _triggerDown)
                {
                    CheckLine();
                }
            }
        }

        private void MoveTarget()
        {
            float distance = _targetCurrentDistance.z / _targetSpeeds.Length;

            for (int i = 0; i < _rayTargets.Length; i++)
            {
                _rayTargets[i].DOMove(_barrelPosition.TransformPoint(Vector3.forward * distance * (i + 1)), _targetSpeeds[i]);
            }

        }

        private void ClampTarget()
        {
            if (_targetCurrentDistance.z > _targetMaxDistance)
                _targetCurrentDistance.z = _targetMaxDistance;
            else if (_targetCurrentDistance.z < _targetMinDistance)
                _targetCurrentDistance.z = _targetMinDistance;
        }

        private void DrawLine()
        {
            _lineRenderer.SetPosition(0, _barrelPosition.position);
            for (int i = 0; i < _rayTargets.Length; i++)
            {
                _lineRenderer.SetPosition(i + 1, _rayTargets[i].position);
            }
            _lineRenderer.SetPosition(_rayTargets.Length + 1, _targetPoint.position);
        }

        private void CheckLine()
        {
            if (_lastHitRigidbody == null)
                return;

            _isHoldingObject = true;
            _heldRigidbody = _lastHitRigidbody;
            _heldRigidbody.velocity = Vector3.zero;

            StartCoroutine(CustomTween(_heldRigidbody, _targetPoint, _objectSnapSpeed, AttachObject));
        }

        private void AttachObject()
        {
            // Catching error if player lets go of object before
            // reaching the target
            if (_heldRigidbody == null)
                return;

            _heldRigidbody.velocity = Vector3.zero;
            _heldRigidbody.transform.position = _targetRigidbody.position;
            _targetFixedJoint = _targetPoint.gameObject.AddComponent<FixedJoint>();
            _targetFixedJoint.connectedBody = _heldRigidbody;
        }

        private void DropObject()
        {
            _heldRigidbody.velocity = (_barrelPosition.TransformPoint(Vector3.forward * _targetCurrentDistance.z) - _heldRigidbody.position) * _throwForce;

            _isHoldingObject = false;
            _heldTweener = null;
            _heldRigidbody = null;
            _lastHitRigidbody = null;
            Destroy(_targetFixedJoint);
            _targetFixedJoint = null;
        }

        private IEnumerator CustomTween(Rigidbody startObject, Transform endObject, float duration, UnityAction callback = null)
        {
            float currentTime = 0;
            Vector3 startPosition = startObject.transform.position;

            while (currentTime < duration)
            {
                startObject.MovePosition(Vector3.Lerp(startPosition, endObject.position, currentTime / duration));
                currentTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();

                if (!_isHoldingObject)
                    currentTime = duration;
            }

            callback?.Invoke();
        }

        private void CheckLength()
        {
            bool raycast = Physics.Raycast(
                _barrelPosition.position,
                _barrelPosition.forward,
                out RaycastHit hitInfo,
                _targetCurrentDistance.z);

            _lastHitRigidbody = null;
            if (!raycast)
                return;

            _lastHitRigidbody = hitInfo.collider.GetComponent<Rigidbody>();

            if (_lastHitRigidbody == null)
            {
                _targetCurrentDistance.z = hitInfo.distance;
                return;
            }
        }
    }

}